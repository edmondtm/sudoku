from utils import *

def grid_values(grid):
    """Convert grid string into {<box>: <value>} dict with '.' value for empties.
    

    Args:
        grid: Sudoku grid in string form, 81 characters long
    Returns:
        Sudoku grid in dictionary form:
        - keys: Box labels, e.g. 'A1'
        - values: Value in corresponding box, e.g. '8', or '.' if it is empty.
    """
    pass

    dict = {}
    grid_count = 0
    for i in cross(rows, cols):
        dict[i] = grid[grid_count]
        grid_count = grid_count + 1

    return dict

def main():
    grid = '..3.2.3..9..3.5..1..18.64....81.29..7.......8..67.82....26.95..8..2.3..9..5.1.3..'
    print grid_values(grid)
    display(grid_values(grid))


if __name__ == '__main__':
  main()